from matplotlib import use
use('agg')
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u

from sim import Simulation
from physics import *
from drivers import *
from solvers.temporal_solvers import *

mu0 = np.pi * 4.0e-7 * (u.newton / (u.amp**2))


def gaussian(x, mean=0.0, std=1.0, amp=1.0):
    """Simple function to return a Gaussian distribution"""
    if isinstance(x, list):
        x = np.array(x)
    power = -((x - mean) ** 2.0) / (2.0 * (std ** 2.0))
    f = amp * np.exp(power)
    if amp == 1:
        f = f / max(f)
    return f


def test_Simulation():
    """
    Basic testing for implementing and configuring an instance of the Simulation class.
    """
    print("test_Simulation()")

    # ======================
    print("- Initiating Simulation instance...", end="")
    grid = (32, 32, 32)
    test_sim = Simulation(xrange=np.linspace(-0.5, 0.5, grid[0]),
                          yrange=np.linspace(-0.5, 0.5, grid[1]),
                          zrange=np.linspace(0, 1.0, grid[2]))
    test_sim.physics.density = np.ones(grid) * u.kg / u.m**3

    assert test_sim.grid_size == grid
    assert test_sim.velocity.shape == (3, *grid)
    assert test_sim.momentum.shape == (3, *grid)
    assert test_sim.density.shape == grid
    assert test_sim.magnetic_field.shape == (3, *grid)
    assert test_sim.energy.shape == grid
    assert test_sim.kinetic_pressure.shape == grid
    assert test_sim.magnetic_pressure.shape == grid
    assert test_sim.pressure.shape == grid
    print("Success")

    # ======================
    print("- Setting parameter values and checking automatically calculated parameters...", end="")
    test_sim.density = np.ones((2, *grid)) * u.kg / u.m**3
    assert test_sim.density.mean() == 1 * u.kg / u.m**3, \
      "Density not successfully set - mean value = {}".format(test_sim.density.mean())

    test_sim.momentum = np.ones((3, *grid)) * u.kg / (u.m**2 * u.s)
    assert test_sim.momentum.mean() == 1 * u.kg / (u.m**2 * u.s), \
      "Momentum not successfuly set - mean value = {}".format(test_sim.momentum.mean())

    test_sim.magnetic_field = np.ones((2, 3, *grid)) * u.Tesla
    assert test_sim.magnetic_field.mean() == 1 * u.Tesla, \
      "Magnetic field not successfuly set - mean value = {}".format(test_sim.magnetic_field.mean())

    test_sim.energy = np.ones((2, *grid)) * u.J / u.m**3
    assert test_sim.energy.mean() == 1 * u.J / u.m**3, \
      "Energy density not successfuly set - mean value = {}".format(test_sim.energy.mean())

    print("Success")


def dan_brown_euler():
    """
    Some basic tests from Dan Brown's ODE-solving notes.
    """

    print('dan_brown_euler()')

    # Define simulation grid and coordinates
    def initiate(tstep, label):
        # Note the 'x' range is actually time
        print('- - Initiating Simulation... ', end='')
        simple = Simulation(xrange=None,
                            yrange=None, zrange=None,
                            physics=EulerTest,
                            time_stepper=tstep, dt=0.25*u.s)
        print('Success')
    
        # Define initial parameter values - only in perturbed component
        print('- - Setting initial conditions... ', end='')
        y0 = np.array([0.3845])
        simple.current_time = -0.1 * u.s
    
        # Set simulation parameters to values defined above
        simple.physics.y = y0
        print('Success')

        return simple

    # Define true solution
    t = np.arange(-0.35, 2.5, 0.05)
    true_y = ((t**3) / 2) - ((3*t**2) / 2) + t + 0.5

    fig, ax = plt.subplots()
    ax.plot(t, true_y, color='black', label='True solution')

    for solver, label in zip([euler_simple, euler_mod, euler_imp],
                             ['Simple Euler', 'Modified Euler', 'Improved Euler']):
        print('- {}'.format(label))
        t = []
        y_sol = []

        simple = initiate(solver, label)
    
        print('- - Running Simulation... ', end='')
        for i in range(11):
            simple.run_simulation(max_its=i)
            y_sol.append(simple.y[0])
            t.append(simple.current_time.value)
        print("Simulation complete")
    
        ax.plot(t, y_sol, marker='x', linestyle='--', label=label)
    plt.legend()
    plt.savefig('tests/dan_brown_euler')
    plt.close()


def dan_brown_rk():
    """
    Some basic tests from Dan Brown's ODE-solving notes.
    """

    print('dan_brown_rk()')

    # Define simulation grid and coordinates
    def initiate(tstep, label):
        # Note the 'x' range is actually time
        print('- - Initiating Simulation... '.format(label), end='')
        simple = Simulation(xrange=None,
                            yrange=None, zrange=None,
                            physics=RKTest,
                            time_stepper=tstep, dt=0.8*u.s)
        print('Success')
    
        # Define initial parameter values - only in perturbed component
        print('- - Setting initial conditions... ', end='')
        y0 = np.array([np.sqrt(2)])
    
        # Set simulation parameters to values defined above
        simple.physics.y = y0
        print('Success')

        return simple

    # Define true solution
    t = np.arange(0, 16*0.8, 0.05)
    true_y = np.sqrt(2.0 + np.sin(t))

    fig, ax = plt.subplots()
    ax.plot(t, true_y, color='black', label='True solution')

    for solver, label in zip([runge_kutta2, runge_kutta4],
                             ['2nd-order RK', '4th-order RK']):
        print('- {}'.format(label))
        t = []
        y_sol = []

        simple = initiate(solver, label)
    
        print('- - Running Simulation... ', end='')
        for i in range(16):
            simple.run_simulation(max_its=i)
            y_sol.append(simple.y[0])
            t.append(simple.current_time.value)
        print("Simulation complete")
    
        ax.plot(t, y_sol, marker='x', linestyle='--', label=label)
    plt.legend()
    plt.savefig('tests/dan_brown_rk')
    plt.close()


def heat_diffusion1d():
    """
    Run basic test for simulating heat diffusion in one dimension.
    """

    print('heat_diffusion1d()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    alpha = 1*u.m**2/u.s
    length = 1 * u.m
    dt = 5.556e-2 * u.s
    r = 0.6
    heatdiff = Simulation(#xrange=np.arange(0, length.value, 5.263e-2),
                          xrange=np.linspace(0, length.value, 21),
                          yrange=None, zrange=None,
                          physics=HeatDiffusion, physpars={'alpha': alpha},
                          dt=dt, time_stepper=euler_simple)
    grid = heatdiff.grid_size
    alpha = (r * (heatdiff.stepsize**2)) / dt
    heatdiff.physics.alpha = alpha
    x = heatdiff.coords[0]
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    heat0 = np.sin(((x*np.pi)/length).value) * u.K

    # Set simulation parameters to values defined above
    heatdiff.physics.heat = heat0.copy()
    print('Success')

    fig, ax = plt.subplots()
    ax.plot(x, heat0, color='black')

    t = [0.21, 0.47, 0.74, 1.0]

    print('- Running Simulation... ', end='')
    for time in t:
        heatdiff.run_simulation(max_time=time*u.s)
        true_heat = heat0 * np.exp((-alpha * (np.pi ** 2) * heatdiff.current_time) / (length**2))
        ax.plot(x, true_heat, color='black')
        ax.plot(x.value, heatdiff.heat, marker='x', linestyle='--', label=r'{:.2f}'.format(heatdiff.current_time))
    print("Simulation complete")

    plt.legend()
    plt.savefig('tests/heat_diffusion1d')
    plt.close()


def heat_diffusion2d():
    """
    Run basic test for simulating heat diffusion in two dimensions.
    """

    print('heat_diffusion2d()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    alpha = 1*u.m**2/u.s
    length = 1 * u.m
    dt = 5.556e-2 * u.s
    r = 0.6
    heatdiff = Simulation(xrange=np.linspace(0, length.value, 21),
                          yrange=np.linspace(0, length.value, 21),
                          zrange=None,
                          physics=HeatDiffusion, physpars={'alpha': alpha},
                          dt=dt, time_stepper=euler_simple)
    grid = heatdiff.grid_size
    ds = heatdiff.stepsize.mean()**2
    alpha = (r * ds) / dt
    heatdiff.physics.alpha = alpha
    x, y = heatdiff.coords
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    heat0 = (np.sin(((x*np.pi)/length).value) + np.sin(((y*np.pi)/length).value)) / 2

    # Set simulation parameters to values defined above
    heatdiff.physics.heat = heat0 * u.K
    print('Success')

    fig, ax = plt.subplots(2, 4, figsize=(18, 12))
    ax[0, 0].imshow(heatdiff.heat.value, origin='lower', cmap='gist_heat', vmin=0, vmax=1)
    ax[1, 0].imshow(heatdiff.heat.value, origin='lower', cmap='gist_heat', vmin=0, vmax=1)

    times = [0.21, 0.47, 0.74, 1.0]

    print('- Running Simulation... ', end='')
    for t, time in enumerate(times):
        heatdiff.run_simulation(max_time=time*u.s)
        true_heat = heat0 * np.exp((-alpha * (np.pi ** 2) * heatdiff.current_time) / (length**2))
        ax[0, t].imshow(true_heat.value, origin='lower', cmap='gist_heat', vmin=0, vmax=1)
        ax[0, t].set_title('Analytical solution, {:.2f}'.format(heatdiff.current_time))
        ax[1, t].imshow(heatdiff.heat.value, origin='lower', cmap='gist_heat', vmin=0, vmax=1)
        ax[1, t].set_title('Numerical solution, {:.2f}'.format(heatdiff.current_time))
    print("Simulation complete")

    plt.savefig('tests/heat_diffusion2d')
    plt.close()


def alfven_wave():
    """
    Test with a torsional Alfven wave input, based on
    http://cds.cern.ch/record/615475/files/0305088.pdf
    """
    print('alfven_wave')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    A = 0.1
    k = (2.0 * np.pi) / (1.0 * u.m)
    omega = np.sqrt(2.0) * k.value / u.s
    waves = Simulation(xrange=np.linspace(0, 1, 64),
                       yrange=np.linspace(0, 1, 64),
                       zrange=np.linspace(0, 1, 64),
                       driver=torsional_alfven(A, k, omega))

    grid = waves.grid_size
    x, y, z = waves.coords
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    density = np.ones(grid) * u.kg / u.m**3
    

    # Set simulation parameters to values defined above
    print('Success')

    print('- Running Simulation... ', end='')
    waves.run_simulation(max_its=1)
    print("Simulation complete")
    print(waves.coords.shape, waves.velocity.shape)

    fig, ax = plt.subplots()
    print(x.shape, waves.velocity.shape)
    x, vel = x[:, 0, 0], waves.velocity[0, :, 0, 0]
    print(x.shape, vel.shape)
    print(x.min(), vel.min())
    ax.plot(x, vel)
    plt.savefig('tests/alfven_waves')
    plt.close()


def pool_waves():
    """
    Run simulation for the 'waves in a pool' problem described here: http://grid.engin.umich.edu/~gtoth/VAC/Man/Examples.html#ExampleA
    """

    print('pool_waves()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    waves = Simulation(xrange=np.linspace(0, 1, 50),
                       yrange=np.linspace(0, 1, 50),
                       physics=AdiabaticHD,
                       physpars={'gamma': 2, 'adiabatic': 4.9},#u.m/(u.s**2)},
                       dt=1e-4*u.s)
    grid = waves.grid_size
    x, y = waves.coords
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    density = np.ones(grid) * u.kg / u.m**3

    density[24:26, 24:26] = 1.3 * density.unit

    # Set simulation parameters to values defined above
    waves.physics.density = density
    print('Success')

    for maxi in range(0, 2000, 100):
        print('- Running Simulation... ', end='')
        waves.run_simulation(max_its=maxi)
        print("Simulation complete")
    
        fig, ax = plt.subplots()
        plt.imshow(waves.density.value, cmap='coolwarm')
        plt.colorbar()
        plt.savefig('tests/pool_waves_{}'.format(maxi))
        plt.close()


def riemann_shock():
    """
    Run simulation for the Riemann shock tube problem.
    """

    print('riemann_shock()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    riemann = Simulation(xrange=np.linspace(0, 1, 128), #256),
                         yrange=None, zrange=None,
                         physpars={'gamma': 1.4},
                         #dt=0.5e-5*u.s)
                         #dt=2.5e-5*u.s)
                         dt=1e-4*u.s)
    grid = riemann.grid_size
    x = riemann.coords[0]
    print(riemann.stepsize, riemann.dt, riemann.dt/(riemann.stepsize[0]**2))
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    density = np.zeros(grid) * u.kg / u.m**3
    energy = np.zeros(grid) * u.J / u.m**3

    discont = 0.5 * u.m
    density[x < discont] = 1.0 * density.unit
    density[x > discont] = 0.125 * density.unit
    energy[x < discont] = 2.5 * energy.unit
    energy[x > discont] = 0.25 * energy.unit

    # Set simulation parameters to values defined above
    riemann.physics.density = density
    riemann.physics.energy = energy
    print('Success')

    for maxt in np.arange(0, 0.21, 0.02):
    #for maxits in range(1, 10):
        print(riemann.stepsize, riemann.dt, riemann.dt/(riemann.stepsize[0]**2))
        print('- Running Simulation... ', end='')
        riemann.run_simulation(max_time=maxt*u.s)
        #riemann.run_simulation(max_its=maxits)
        #print(riemann.current_iteration, riemann.current_time, riemann.dt)
        #for v in riemann.physics.core_variables:
        #    print(v.mean())
        print("Simulation complete")

        fig, ax = plt.subplots(2, 2)
        ax = ax.flatten()
        ax[0].plot(x, riemann.density)
        ax[0].set_ylim(0, 1.2)
        ax[1].plot(x, riemann.velocity[0, :, ...])
        ax[1].set_ylim(0, 1.1)
        ax[2].plot(x, riemann.physics.thermal_energy)
        ax[3].plot(x, riemann.pressure)
        ax[3].set_ylim(0, 1.2)
        #ax[3].plot(x, riemann.physics.total_viscosity(riemann.density))
        plt.savefig('tests/riemann_shock_{:.4f}'.format(maxt).replace('.', '_'))
        #plt.savefig('tests/riemann_shock_{:03}'.format(maxits))
        plt.close()

        if riemann.dt.value < 1e-100:
            break


def strong_shock():
    """
    Run simulation for the strong shock tube problem.
    """

    print('strong_shock()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    strong = Simulation(xrange=np.linspace(0, 1, 32),
                        yrange=None, zrange=None,
                        gamma=1.4)
    grid = strong.grid_size
    x = strong.coords[0]
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    density = np.zeros((2, *grid)) * u.kg / u.m**3
    energy = np.zeros((2, *grid)) * u.J / u.m**3

    discont = 0.5 * u.Mm
    density[1, x > discont] = 0.125 * density.unit
    density[1, x < discont] = 10.0 * density.unit
    energy[1, x < discont] = 2.5 * energy.unit
    energy[1, x > discont] = 0.25 * energy.unit

    # Set simulation parameters to values defined above
    strong.density = density
    strong.energy = energy
    print('Success')

    print('- Running Simulation... ', end='')
    strong.run(max_time=0.12*u.s)
    print("Simulation complete")

    fig, ax = plt.subplots(2, 2)
    ax = ax.flatten()
    ax[0].plot(x, strong.density.sum(axis=0))
    ax[1].plot(x, strong.velocity[0, :, ...])
    ax[2].plot(x, strong.energy.sum(axis=0))
    ax[3].plot(x, strong.total_pressure.sum(axis=0))
    plt.savefig('tests/strong_shock')
    plt.close()


def briowu_shock():
    """
    Run simulation for the Brio & Wu shock tube problem.
    """

    print('briowu_shock()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    briowu = Simulation(xrange=np.linspace(0, 1, 32),
                        yrange=None, zrange=None,
                        gamma=2.0)
    grid = briowu.grid_size
    x = briowu.coords[0]
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    density = np.zeros((2, *grid)) * u.kg / u.m**3
    energy = np.zeros((2, *grid)) * u.J / u.m**3

    discont = 0.5 * u.Mm
    density[1, x > discont] = 0.125 * density.unit
    density[1, x < discont] = 1.0 * density.unit
    energy[1, x < discont] = 2.5 * energy.unit
    energy[1, x > discont] = 0.25 * energy.unit

    # Set simulation parameters to values defined above
    briowu.density = density
    briowu.energy = energy
    briowu.magnetic_field[1, 0, :] = 0.75 * u.Tesla
    briowu.magnetic_field[1, 1, x < discont] = 1 * u.Tesla
    briowu.magnetic_field[1, 1, x > discont] = -1 * u.Tesla
    print('Success')

    print('- Running Simulation... ', end='')
    briowu.run(max_time=0.11*u.s)
    print("Simulation complete")

    fig, ax = plt.subplots(2, 2)
    ax = ax.flatten()
    ax[0].plot(x, briowu.density.sum(axis=0))
    ax[1].plot(x, briowu.velocity[0, :, ...])
    ax[2].plot(x, briowu.total_pressure.sum(axis=0))
    ax[3].plot(x, briowu.magnetic_field.sum(axis=0)[1])
    plt.savefig('tests/briowu_shock')
    plt.close()


def orszag_tang_vortex():
    """
    Run simulation for the Brio & Wu shock tube problem.
    """

    print('orszag_tang_vortex()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    otvort = Simulation(xrange=np.linspace(0, 1, 32),
                        yrange=np.linspace(0, 1, 32),
                        zrange=None)
    grid = otvort.grid_size
    x, y = otvort.coords
    print('Success')

    # Define initial parameter values - only in perturbed component
    # TODO: Add energy based on pressure specified in paper
    print('- Setting initial conditions... ', end='')
    #density = np.ones(grid) * u.kg / u.m**3
    density = (25/36) * np.pi * u.kg / u.m**3
    #energy = np.zeros((2, *grid)) * u.J / u.m**3
    B0 = 1 * u.Tesla#mu0 / np.sqrt(4 * np.pi) * u.Tesla
    Bx = -B0 * np.sin(4*np.pi*y.value)
    By = B0 * np.sin(2*np.pi*x.value)
    vx = np.sin(2*np.pi*y.value) * u.m / u.s
    vy = np.sin(2*np.pi*x.value) * u.m / u.s

    # Set simulation parameters to values defined above
    otvort.density[0] = density
    otvort.magnetic_field[0, 0, :] = Bx
    otvort.magnetic_field[0, 1, :] = By
    otvort.momentum[0] = vx * density
    otvort.momentum[1] = vy * density
    print('Success')

    print('- Running Simulation... ', end='')
    otvort.run(max_time=0.5*u.s)
    print("Simulation complete")

    fig, ax = plt.subplots()
    ax.imshow(otvort.density.sum(axis=0).value, origin='lower', cmap='plasma')
    plt.savefig('tests/orszag_tang_vortex')
    plt.close()


def kelvin_helmholtz_hd():
    """
    Run simulation for the Kelvin-Helmholtz instability problem.
    """

    print('kelvin_helmholtz()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    kh = Simulation(xrange=np.linspace(-0.5, 0.5, 128),
                    yrange=np.linspace(-0.5, 0.5, 128),
                    zrange=None,
                    physics=HD, #physpars={}
                    time_stepper=euler_simple,
                    dt=3e-5*u.s)
    grid = kh.grid_size
    x, y = kh.coords
    print('Success')

    print('- Setting initial conditions... ', end='')
    #density = np.ones(y.shape) * (u.kg / u.m**3)
    #density[np.where(abs(y.value) <= 0.25)] = 2 * (u.kg / u.m**3)
    y1 = (y.value - y.min().value) * np.pi
    density = (np.sin(y1) + 2) * u.kg / u.m**3
    print(density.min(), density.max())

    vel = np.zeros((3, *y.shape)) * u.m / u.s
    vel[0] = 0.5 * u.m / u.s
    vel[0][np.where(abs(y.value) > 0.25)] = -0.5 * u.m / u.s
    #vel[0] = np.sin(y1) * u.m / u.s

    #vel += np.random.rand(*vel.shape) * u.m / u.s
    print(vel.shape, vel.max())

    kh.physics.density = density
    kh.physics.velocity = vel
    print(kh.velocity.mean())
    #kh.physics.velocity[0] = vx
    #kh.physics.velocity[1] = vy
    print('Success')

    print('- Running Simulation... ', end='')
    #kh.run_simulation(max_its=10000)
    #print("Simulation complete")

    """fig, ax = plt.subplots()
    ax.imshow(kh.density.value, origin='lower', cmap='plasma')
    plt.savefig('tests/kelvin_helmholtz_hd')
    plt.close()"""
    for maxt in np.arange(0, 5.1, 0.1):
        print(kh.stepsize, kh.dt, kh.dt/(kh.stepsize[0]**2))
        print('- Running Simulation... ', end='')
        kh.run_simulation(max_time=maxt*u.s)
        print(kh.current_iteration, kh.current_time, kh.dt, kh.velocity.mean())
        print("Simulation complete")
    
        fig, ax = plt.subplots()
        #ax.imshow(kh.density.value, origin='lower', cmap='plasma')#, vmin=0.9, vmax=2.1)
        ax.imshow(kh.velocity[0].value, origin='lower', cmap='coolwarm')
        plt.savefig('tests/kelvin_helmholtz_hd_{:.2f}'.format(maxt).replace('.', '-'))
        plt.close()


def mhd_rotor():
    """
    Run simulation for the MHD rotor simulation.
    """

    print('mhd_rotor()')

    r0 = 0.1
    r1 = 0.115

    def func(r, r0=r0, r1=r1):
        return (r1 - r) / (r - r0)

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    mhdr = Simulation(xrange=np.linspace(-0.5, 0.5, 512),
                      yrange=np.linspace(-0.5, 0.5, 512),
                      zrange=None,
                      physics=MHD, physpars={'gamma': 5/3},
                      time_stepper=euler_simple,
                      dt=3e-5*u.s)
    grid = mhdr.grid_size
    x, y = mhdr.coords
    r = np.sqrt(x**2 + y**2).value
    fr = np.zeros(r.shape)
    idx = (r != r0)
    fr[idx] = (r1 - r[idx]) / (r[idx] - r0)
    print('Success')

    print('- Setting initial conditions... ', end='')
    density = 1 + (9 * fr)
    density[np.where(r >= r1)] = 1
    density[np.where(r <= r0)] = 10

    vel = np.zeros((3, *r.shape))
    vel[0][np.where(r < r1)] = (-fr * y / r)[np.where(r < r1)]
    vel[0][np.where(r <= r0)] = (-fr * y / r0)[np.where(r <= r0)]
    vel[1][np.where(r < r1)] = (fr * x / r)[np.where(r < r1)]
    vel[1][np.where(r <= r0)] = (fr * x / r0)[np.where(r <= r0)]

    print(vel.shape, vel.max())

    bfield = np.zeros((3, *r.shape)) * u.T
    bfield[0] = (5 / np.sqrt(4*np.pi)) * u.T

    mhdr.physics.density = density * u.kg / u.m**3
    mhdr.physics.velocity = vel * u.m / u.s
    mhdr.physics.magnetic_field = bfield

    energy = (1 * u.N / u.m**2) + ((mhdr.density * (mhdr.velocity[0]**2 + mhdr.velocity[1]**2)) / 2)
    energy /= mhdr.physics.gamma - 1
    mhdr.physics.energy = energy
    print(mhdr.energy.mean(), mhdr.pressure.mean())

    print(mhdr.velocity.mean())
    print('Success')

    print('- Running Simulation... ')
    mhdr.run_simulation(max_time=0*u.s)#.15)
    print("Simulation complete")

    fig, ax = plt.subplots()
    ax.imshow(mhdr.density.value, origin='lower', cmap='plasma', vmin=0, vmax=12)
    plt.savefig('tests/mhd_rotor')
    plt.close()
    """for maxt in np.arange(0, 5.1, 0.1):
        print(kh.stepsize, kh.dt, kh.dt/(kh.stepsize[0]**2))
        print('- Running Simulation... ', end='')
        kh.run_simulation(max_time=maxt*u.s)
        print(kh.current_iteration, kh.current_time, kh.dt, kh.velocity.mean())
        print("Simulation complete")
    
        fig, ax = plt.subplots()
        ax.imshow(kh.density.value, origin='lower', cmap='plasma', vmin=0.9, vmax=2.1)
        plt.savefig('tests/kelvin_helmholtz_hd_{:.2f}'.format(maxt).replace('.', '-'))
        plt.close()"""


def mhd_waves():
    """
    """

    print('mhd_waves()')

    # Define simulation grid and coordinates
    print('- Initiating Simulation... ', end='')
    waves = Simulation(xrange=np.linspace(-0.5, 0.5, 128),
                       yrange=np.linspace(-0.5, 0.5, 128),
                       physics=MHD,
#                       physpars={'gamma': 2, 'adiabatic': 4.9},#u.m/(u.s**2)},
                       dt=1e-4*u.s)
    grid = waves.grid_size
    x, y = waves.coords
    r = np.sqrt(x**2 + y**2)
    print('Success')

    # Define initial parameter values - only in perturbed component
    print('- Setting initial conditions... ', end='')
    bfield = np.zeros((3, *grid)) * u.T
    #bfield[1] = 0.2 * u.mT
    density = (gaussian(r.value, std=0.06, amp=0.9) + 0.1) * u.kg / u.m**3
    energy = (gaussian(r.value, std=0.06, amp=0.9) + 0.1) * u.J / u.m**3

    waves.physics.density = density
    waves.physics.energy = energy
    waves.physics.magnetic_field = bfield
    print('Success')

    for maxi in range(0, 5001, 1000):
        print('- Running Simulation... ', end='')
        waves.run_simulation(max_its=maxi)
        print("Simulation complete")
    
        fig, ax = plt.subplots()
        plt.imshow(waves.density.value, cmap='plasma')
        plt.colorbar()
        plt.savefig('tests/mhd_waves_{}'.format(maxi))
        plt.close()


if __name__ == "__main__":
    test_Simulation()
    dan_brown_euler()
    dan_brown_rk()
    heat_diffusion1d()
    heat_diffusion2d()
    #alfven_wave()
    #pool_waves()
    #riemann_shock()
    #strong_shock()
    #briowu_shock()
    #orszag_tang_vortex()
    #kelvin_helmholtz_hd()
    #mhd_rotor()
    mhd_waves()
