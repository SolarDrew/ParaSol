import numpy as np
from operators import *
import astropy.units as u
from solvers.spatial_solvers import Solver


class EulerTest():
    def __init__(self, grid_size):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self._y = np.zeros(grid_size)

        self._variable_names = ['y']

        self.equations = [self._ddt_y]

    def _ddt_y(self, t, y=None):
        if not y:
            y = self.y
        t = t.value
        return (((3*t**2) / 2) - (3*t) + 1) / u.s

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    @property
    def core_variables(self):
        """
        """
        return [self.y]


class RKTest():
    def __init__(self, grid_size):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self._y = np.zeros(grid_size)

        self._variable_names = ['y']

        self.equations = [self._ddt_y]

    def _ddt_y(self, t, y=None):
        if not y:
            y = self.y
        t = t.value
        return (np.cos(t) / (2 * y)) / u.s

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    @property
    def core_variables(self):
        """
        """
        return [self.y]


class HeatDiffusion():
    @u.quantity_input
    def __init__(self, grid_size, alpha: u.m**2 / u.s):
        """
        """
        assert alpha > 0, 'Diffusion constant alpha must be positive'

        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.alpha = alpha

        self._heat = np.zeros(grid_size) * u.K

        self._variable_names = ['heat']

        self.equations = [self._ddt_heat]

    def _ddt_heat(self, t, heat=None):
        if not heat:
            heat = self.heat
        return self.alpha * set_boundaries(laplacian(heat, Solver(self.solver.dx, 'central', 2, 2)))

    @property
    def heat(self):
        return self._heat

    @heat.setter
    @u.quantity_input
    def heat(self, heat_arr: u.K):
        assert heat_arr.shape == self.grid_size, 'Specified array shape does not match simulation grid'
        self._heat = heat_arr

    @property
    def core_variables(self):
        """
        """
        return [self.heat]
