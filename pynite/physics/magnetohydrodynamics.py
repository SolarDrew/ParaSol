from operators import *
import astropy.units as u

mu0 = np.pi * 4.0e-7 * (u.newton / (u.amp**2))


class MHD():
    """Physics class for hydrodynamics.

    This class defines the Euler equations for fluid dynamics:

    .. math::

       \\frac{\\partial \\rho}{\\partial t} + \\nabla \\cdot (\\vec{v} \\rho) = 0
       \\frac{\\partial (\\rho \vec{v})}{\\partial t} + \\nabla \\cdot (\\vec{v} \\rho \\vec{v}) + \\nabla p = 0
       \\frac{\\partial e}{\\partial t} + \\nabla \\cdot (\\vec{v} e + \\vec{v} p) = 0

    for the fluid density, :math:`\\rho`, momentum, :math:`\\vec{m} = \\vec{v} \\rho`, energy :math:`e` and kinetic pressure :math:`p`.
    The pressure is a derived quantity defined as

    .. math::
       p = (\\gamma - 1) (e - \\frac{\\rho \\vec{v}^2}{2})

    Parameters
    ----------
    grid_size : tuple of ints
        Tuple of 1, 2 or 3 values defining the size of the simulation grid.
    gamma : float
        Value of the adiabatic index.

    Attributes
    ----------
    grid_size : tuple of ints
        Size of the simulation grid, as defined at initiation.
    gamma : float
        Adiabatic index for the simulation.

    See Also
    --------
    Base physics class?

    References
    ----------

    Examples
    --------

    """
    def __init__(self, grid_size, gamma=5/3):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.gamma = gamma

        # Initiate empty arrays for core variables
        self._density = np.zeros(grid_size) * u.kg / u.m**3
        self._momentum = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)
        self._energy = np.zeros(grid_size) * u.J / u.m**3
        self._magnetic_field = np.zeros((3, *grid_size)) * u.Tesla

        # Collect names of all variables into some lists
        self._variable_names = ['density', 'momentum', 'energy', 'magnetic_field', # Core equation variables
                                'velocity', 'magnetic_pressure', 'kinetic_pressure', 'pressure'] # Derived variables

        # Collect equations into a nice easy-to-use list
        self.equations = [self._ddt_density, self._ddt_momentum, self._ddt_energy, self._ddt_magfield]

    def _ddt_density(self, t, density=None):
        """
        """
        if not density:
            density = self.density
        return -div(self.velocity * density, self.solver)

    def _ddt_momentum(self, t, momentum=None):
        """
        """
        if not momentum:
            momentum = self.momentum
        v = self.velocity
        B = self.magnetic_field / np.sqrt(mu0)

        return (-grad(self.pressure, self.solver) \
                - tensordiv(vdp(v, momentum) - vdp(B, B), self.solver)) # momentum * rho?

    def _ddt_energy(self, t, energy=None):
        """
        """
        if not energy:
            energy = self.energy
        v = self.velocity
        B = self.magnetic_field / np.sqrt(mu0)
        
        return -div((v*energy) - (B * dot(B, v)) + (v*self.pressure), self.solver)

    def _ddt_magfield(self, t, magfield=None):
        """
        """
        if not magfield:
            B = self.magnetic_field / np.sqrt(mu0)
        else:
            B = magfield / np.sqrt(mu0)
        v = self.velocity
        
        return -tensordiv(vdp(v, B) - vdp(B, v), self.solver) * np.sqrt(mu0)

    """
    Define getters and setters for variables.
    """
    ## ==== Core variables ====
    # Density
    @property
    def density(self):
        return self._density

    @density.setter
    @u.quantity_input
    def density(self, density: u.kg/u.m**3):
        """Sets the simulation's density profile to the specified array.
        Other arrays which depend on the density values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        density : ndarray
            Array of density values. Shape and size must be equal to those of the simulation grid.
            Must have units of density.

        """

        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density = density

    # Momentum
    @property
    def momentum(self):
        return self._momentum

    @momentum.setter
    @u.quantity_input
    def momentum(self, momentum: u.kg/(u.m**2 * u.s)):
        """Sets the simulation's momentum profile to the specified array.
        Other arrays which depend on the velocity values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        momentum : ndarray
            Array of momentum vectors. Shape must be (3, x, [y, z]), where x, y and z are the dimensions of the simulation grid.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """
        assert momentum.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._momentum = momentum

    # Internal energy
    @property
    def energy(self):
        return self._energy

    @energy.setter
    @u.quantity_input
    def energy(self, energy: u.J/u.m**3):
        """Sets the simulation's total energy density profile to the specified array.
        Other arrays which depend on the energy values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        energy : ndarray
            Array of energy values. Shape must be (x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Must have units of energy.

        """

        assert energy.shape == self.grid_size, \
            'Specified energy density array shape does not match simulation grid.'
        shape, unit = self._energy.shape, self._energy.unit
        self._energy = energy

    # Magnetic field
    @property
    def magnetic_field(self):
        return self._magnetic_field

    @magnetic_field.setter
    @u.quantity_input
    def magnetic_field(self, magnetic_field: u.Tesla):
        """ 
        Sets the simulation's magnetic field profile to the specified array.
        Other arrays which depend on the magnetic field, such as the magnetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        magnetic_field : ndarray
            Array of magnetic field values. Shape must be (3, x, [y, z]), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """

        assert magnetic_field.shape == (3, *self.grid_size), \
            'Specified magnetic field array shape does not match simulation grid.'
        self._magnetic_field = magnetic_field

    @property
    def core_variables(self):
        """Returns an up-to-date list of the core variables used in the calculations.

        """
        return [self.density, self.momentum, self.energy, self.magnetic_field]

    ## ==== Derived variables ====
    # Velocity
    @property
    def velocity(self):
        """Returns the velocity profile of the simulation, as calculated from the momentum and total density.
        """

        return self.momentum / self.density

    @velocity.setter
    @u.quantity_input
    def velocity(self, velocity: u.m / u.s):
        """Defines the velocity throughout the simulation, and automatically updates the momentum based on the current density values.

        Parameters
        ----------

        velocity : ndarray
            Array of velocity vectors with shape (3, x, [y, z]) where x, y and z are the spatial grid sizes of the simulation.
            Note that a full 3D vector is required even if the simulation is run for fewer than 3 dimensions.
            Must have units of velocity.

        """
        assert velocity.shape == (3, *self.grid_size), \
          'Specified velocity array shape does not match simulation grid.'
        self.momentum = velocity * self.density

    @property
    def magnetic_pressure(self):
        """
        """
        B = self.magnetic_field / np.sqrt(mu0)

        return dot(B, B) / 2

    @property
    def kinetic_pressure(self):
        """Sets the simulation's kinetic pressure profile to the specified array.

        The kinetic pressure is defined as:

        .. math::

            p = (\\gamma - 1) (e_0 - \\frac{\\rho\\textbf{v}^2}{2})
        """
        v = self.velocity

        return (self.gamma - 1) * (self.energy - ((self.density * dot(v, v)) / 2) - self.magnetic_pressure)

    @property
    def pressure(self):

        return self.kinetic_pressure + self.magnetic_pressure

    @property
    def sound_speed(self):
        """Calculate the sound speed everywhere in the domain based on the pressure, density and adiabatic
        index:

        .. math::

            c_s = \\sqrt{\\frac{\\gamma p}{\\rho}}

        """

        return np.sqrt((self.gamma * self.pressure) / self.density)


class MHD_maybe_broken():
    """
    Basic Magnetohydrodynamic physics class.
    """
    def __init__(self, grid_size, gamma=5/3):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.gamma = gamma

        # Initiate empty arrays for core variables
        self._density = np.zeros(grid_size) * u.kg / u.m**3
        self._momentum = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)
        self._magnetic_field = np.zeros((3, *grid_size)) * u.Tesla
        self._energy = np.zeros(grid_size) * u.J / u.m**3

        # Collect names of all variables into a list
        # This is used to allow the parent Simulation object to access these variables.
        self._variable_names = ['density', 'momentum', 'magnetic_field', 'energy', # Core
                                'velocity', 'magnetic_pressure', 'kinetic_pressure', 'pressure'] # Derived

        # Collect appropriate equations into a nice easy-to-use list
        self.equations = [self._ddt_density, self._ddt_momentum, self._ddt_energy, self._ddt_magfield]

    def _ddt_density(self, t, density=None):
        """
        """
        if not density:
            density = self.density

        return - div(self.momentum, self.solver)

    def _ddt_momentum(self, t, momentum=None):
        """
        """
        if not momentum:
            momentum = self.momentum
        B = self.magnetic_field / np.sqrt(mu0)
        v = self.velocity

        return (- grad(self.pressure, self.solver) \
                - tensordiv(vdp(v, momentum) - vdp(B, B), self.solver))

    def _ddt_energy(self, t, energy=None):
        """
        """
        if not energy:
            energy = self.energy
        B = self.magnetic_field / np.sqrt(mu0)
        v = self.velocity

        return - div((v*energy) - (B * dot(B, v)) + (v * self.pressure), self.solver)

    def _ddt_magfield(self, t, magfield=None):
        """
        """
        if not magfield:
            B = self.magnetic_field / np.sqrt(mu0)
        else:
            B = magfield / np.sqrt(mu0)
        v = self.velocity

        return -tensordiv(vdp(v, B) - vdp(B, v), self.solver) * np.sqrt(mu0)

    """
    Define getters and setters for variables.
    """
    ## ==== Core variables ====
    # Density
    @property
    def density(self):
        return self._density

    @density.setter
    @u.quantity_input
    def density(self, density: u.kg/u.m**3):
        """Sets the simulation's density profile to the specified array.
        Other arrays which depend on the density values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        density : ndarray
            Array of density values. Shape and size must be equal to those of the simulation grid.
            Must have units of density.

        """

        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density = density

    # Momentum
    @property
    def momentum(self):
        return self._momentum

    @momentum.setter
    @u.quantity_input
    def momentum(self, momentum: u.kg/(u.m**2 * u.s)):
        """Sets the simulation's momentum profile to the specified array.
        Other arrays which depend on the velocity values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        momentum : ndarray
            Array of momentum vectors. Shape must be (3, x, [y, z]), where x, y and z are the dimensions of the simulation grid.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """
        assert momentum.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._momentum = momentum

    # Magnetic field
    @property
    def magnetic_field(self):
        return self._magnetic_field

    @magnetic_field.setter
    @u.quantity_input
    def magnetic_field(self, magnetic_field: u.Tesla):
        """ 
        Sets the simulation's magnetic field profile to the specified array.
        Other arrays which depend on the magnetic field, such as the magnetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        magnetic_field : ndarray
            Array of magnetic field values. Shape must be (3, x, [y, z]), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """

        assert magnetic_field.shape == (3, *self.grid_size), \
            'Specified magnetic field array shape does not match simulation grid.'
        self._magnetic_field = magnetic_field

    # Internal energy
    @property
    def energy(self):
        return self._energy

    @energy.setter
    @u.quantity_input
    def energy(self, energy: u.J/u.m**3):
        """ 
        Sets the simulation's total energy density profile to the specified array.
        Other arrays which depend on the energy values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        energy : ndarray
            Array of energy values. Shape must be (x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Must have units of energy.

        """

        assert energy.shape == self.grid_size, \
            'Specified energy density array shape does not match simulation grid.'
        shape, unit = self._energy.shape, self._energy.unit
        self._energy = energy

    @property
    def core_variables(self):
        """ 
        Returns an up-to-date list of the core variables used in the calculations.
        """
        return [self.density, self.momentum, self.energy, self.magnetic_field]

    ## ==== Derived variables ====
    # Velocity
    @property
    def velocity(self):
        """Returns the velocity profile of the simulation, as calculated from the momentum and total density.
        """

        return self.momentum / self.density

    @velocity.setter
    @u.quantity_input
    def velocity(self, velocity: u.m / u.s):
        """Defines the velocity throughout the simulation, and automatically updates the momentum based on the current density values.

        Parameters
        ----------

        velocity : ndarray
            Array of velocity vectors with shape (3, x, [y, z]) where x, y and z are the spatial grid sizes of the simulation.
            Note that a full 3D vector is required even if the simulation is run for fewer than 3 dimensions.
            Must have units of velocity.

        """
        assert velocity.shape == (3, *self.grid_size), \
          'Specified velocity array shape does not match simulation grid.'
        self.momentum = velocity * self.density

    # Thermal energy
    @property
    def thermal_energy(self):
        """ 
        """
        v = self.velocity

        return (self.energy - ((self.density * dot(v, v)) / 2) - self.magnetic_pressure)

    # Magnetic pressure
    @property
    def magnetic_pressure(self):
        """Returns the simulation's magnetic pressure profile.

        The magnetic pressure is defined as:

        .. math::

            \\frac{\\textbf{B}^2}{2 \\mu_0}

        where :math:`\\textbf{B}` is the magnetic field and :math:`\\mu_0` is the permeability of free space.
        TODO: double-check that this is in fact all the case.

        """
        B = self.magnetic_field / np.sqrt(mu0)

        return dot(B, B) / 2

    @property
    def kinetic_pressure(self):
        """Sets the simulation's kinetic pressure profile to the specified array.
        The total pressure, which depends on the kinetic pressure, is redefined automatically.

        The kinetic pressure is defined as:

        .. math::

            (\\gamma - 1) (e_0 - \\frac{\\rho\\textbf{v}^2}{2} - \\frac{\\textbf{B}^2}{2 \\mu_0})
        
        # TODO: double-check the maths again and stick some more description here
        Also check the maths below
        """

        return (self.gamma - 1) * self.thermal_energy

    # Total pressure
    @property
    def pressure(self):
        """Returns the total pressure in the simulation domain by adding the kinetic and magnetic pressures.
        """
        
        return self.kinetic_pressure + self.magnetic_pressure

    @property
    def speed(self):
        return np.sqrt((self.velocity**2).sum(axis=0))

    @property
    def sound_speed(self):
        """Calculate the sound speed everywhere in the domain based on the pressure, density and adiabatic
        index:

        .. math::

            c_s = \\sqrt{\\frac{\\gamma p}{\\rho}}

        """

        #print(self.gamma, self.pressure.min(), self.density.min())
        return np.sqrt((self.gamma * self.pressure) / self.density)

    @property
    def alfven_speed(self):
        """ 
        """
        
        B = self.magnetic_field

        return np.sqrt(dot(B, B) / (mu0 * self.density))


class MHD_imp():
    """
    'Improved' MHD physics class. Includes hyperviscosity terms from Shelyag et al.
    """
    def __init__(self, grid_size, gamma=5/3):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.gamma = gamma

        # Initiate empty arrays for core variables
        self._density = np.zeros(grid_size) * u.kg / u.m**3
        self._momentum = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)
        self._magnetic_field = np.zeros((3, *grid_size)) * u.Tesla
        self._energy = np.zeros(grid_size) * u.J / u.m**3

        # Collect names of all variables into a list
        # This is used to allow the parent Simulation object to access these variables.
        self._variable_names = ['density', 'momentum', 'magnetic_field', 'energy', # Core
                                'velocity', 'magnetic_pressure', 'kinetic_pressure', 'pressure'] # Derived

        # Collect appropriate equations into a nice easy-to-use list
        self.equations = [self._ddt_density, self._ddt_momentum, self._ddt_energy, self._ddt_magfield]

    def _ddt_density(self, t, density=None):
        """
        """
        if not density:
            density = self.density
        nu = self.total_viscosity(self.density)
        D_rho = 0#self.solver(nu, 0) * self.solver(self.density, 0)

        return D_rho - div(self.momentum, self.solver)

    def _ddt_momentum(self, t, momentum=None):
        """
        """
        if not momentum:
            momentum = self.momentum
        B = self.magnetic_field / np.sqrt(mu0)
        v = self.velocity

        D_mom = tensordiv(self.viscous_tensor, self.solver)

        return (D_mom - grad(self.pressure, self.solver) \
                - tensordiv(vdp(v, momentum) - vdp(B, B), self.solver))

    def _ddt_energy(self, t, energy=None):
        """
        """
        if not energy:
            energy = self.energy
        B = self.magnetic_field / np.sqrt(mu0)
        v = self.velocity
        #eps = self.epsilon

        d_visc = div(vt_dot(self.velocity, self.viscous_tensor), self.solver)
        #print('\nd_visc', d_visc.unit.si)
        #print('div(ve)', div(v*energy, self.solver).unit.si)
        nu = self.total_viscosity(self.thermal_energy)
        d_diff = self.solver(nu, 0) * self.solver(self.energy, 0)
        #A = cross(self.magnetic_field/np.sqrt(mu0), eps)
        #print('\n\n', 'A', A.shape, A.unit.si)
        #d_ohm = div(cross(A, eps), self.solver)
        #print('\n\n', d_visc.unit.si, d_diff.unit.si, d_ohm.unit.si)
        #print(eps.shape, eps.unit.si)
        
        D_e = 0#d_visc + d_diff# + d_ohm
        
        return D_e - div((v*energy) - (B * dot(B, v)) + (v * self.pressure), self.solver)

    def _ddt_magfield(self, t, magfield=None):
        """
        """
        if not magfield:
            B = self.magnetic_field / np.sqrt(mu0)
        else:
            B = magfield / np.sqrt(mu0)
        v = self.velocity

        return -tensordiv(vdp(v, B) - vdp(B, v), self.solver) * np.sqrt(mu0)

    """
    Define getters and setters for variables.
    """
    ## ==== Core variables ====
    # Density
    @property
    def density(self):
        return self._density

    @density.setter
    @u.quantity_input
    def density(self, density: u.kg/u.m**3):
        """Sets the simulation's density profile to the specified array.
        Other arrays which depend on the density values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        density : ndarray
            Array of density values. Shape and size must be equal to those of the simulation grid.
            Must have units of density.

        """

        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density = density

    # Momentum
    @property
    def momentum(self):
        return self._momentum

    @momentum.setter
    @u.quantity_input
    def momentum(self, momentum: u.kg/(u.m**2 * u.s)):
        """Sets the simulation's momentum profile to the specified array.
        Other arrays which depend on the velocity values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        momentum : ndarray
            Array of momentum vectors. Shape must be (3, x, [y, z]), where x, y and z are the dimensions of the simulation grid.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """
        assert momentum.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._momentum = momentum

    # Magnetic field
    @property
    def magnetic_field(self):
        return self._magnetic_field

    @magnetic_field.setter
    @u.quantity_input
    def magnetic_field(self, magnetic_field: u.Tesla):
        """ 
        Sets the simulation's magnetic field profile to the specified array.
        Other arrays which depend on the magnetic field, such as the magnetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        magnetic_field : ndarray
            Array of magnetic field values. Shape must be (3, x, [y, z]), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """

        assert magnetic_field.shape == (3, *self.grid_size), \
            'Specified magnetic field array shape does not match simulation grid.'
        self._magnetic_field = magnetic_field

    # Internal energy
    @property
    def energy(self):
        return self._energy

    @energy.setter
    @u.quantity_input
    def energy(self, energy: u.J/u.m**3):
        """ 
        Sets the simulation's total energy density profile to the specified array.
        Other arrays which depend on the energy values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        energy : ndarray
            Array of energy values. Shape must be (x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Must have units of energy.

        """

        assert energy.shape == self.grid_size, \
            'Specified energy density array shape does not match simulation grid.'
        shape, unit = self._energy.shape, self._energy.unit
        self._energy = energy

    @property
    def core_variables(self):
        """ 
        Returns an up-to-date list of the core variables used in the calculations.
        """
        return [self.density, self.momentum, self.energy, self.magnetic_field]

    ## ==== Derived variables ====
    # Velocity
    @property
    def velocity(self):
        """Returns the velocity profile of the simulation, as calculated from the momentum and total density.
        """

        return self.momentum / self.density

    @velocity.setter
    @u.quantity_input
    def velocity(self, velocity: u.m / u.s):
        """Defines the velocity throughout the simulation, and automatically updates the momentum based on the current density values.

        Parameters
        ----------

        velocity : ndarray
            Array of velocity vectors with shape (3, x, [y, z]) where x, y and z are the spatial grid sizes of the simulation.
            Note that a full 3D vector is required even if the simulation is run for fewer than 3 dimensions.
            Must have units of velocity.

        """
        assert velocity.shape == (3, *self.grid_size), \
          'Specified velocity array shape does not match simulation grid.'
        self.momentum = velocity * self.density

    # Thermal energy
    @property
    def thermal_energy(self):
        """ 
        """
        v = self.velocity

        return (self.energy - ((self.density * dot(v, v)) / 2) - self.magnetic_pressure)

    # Magnetic pressure
    @property
    def magnetic_pressure(self):
        """Returns the simulation's magnetic pressure profile.

        The magnetic pressure is defined as:

        .. math::

            \\frac{\\textbf{B}^2}{2 \\mu_0}

        where :math:`\\textbf{B}` is the magnetic field and :math:`\\mu_0` is the permeability of free space.
        TODO: double-check that this is in fact all the case.

        """
        B = self.magnetic_field / np.sqrt(mu0)

        return dot(B, B) / 2

    @property
    def kinetic_pressure(self):
        """Sets the simulation's kinetic pressure profile to the specified array.
        The total pressure, which depends on the kinetic pressure, is redefined automatically.

        The kinetic pressure is defined as:

        .. math::

            (\\gamma - 1) (e_0 - \\frac{\\rho\\textbf{v}^2}{2} - \\frac{\\textbf{B}^2}{2 \\mu_0})
        
        # TODO: double-check the maths again and stick some more description here
        Also check the maths below
        """

        return (self.gamma - 1) * self.thermal_energy

    # Total pressure
    @property
    def pressure(self):
        """Returns the total pressure in the simulation domain by adding the kinetic and magnetic pressures.
        """
        
        return self.kinetic_pressure + self.magnetic_pressure

    @property
    def speed(self):
        return np.sqrt((self.velocity**2).sum(axis=0))

    @property
    def sound_speed(self):
        """Calculate the sound speed everywhere in the domain based on the pressure, density and adiabatic
        index:

        .. math::

            c_s = \\sqrt{\\frac{\\gamma p}{\\rho}}

        """

        #print(self.gamma, self.pressure.min(), self.density.min())
        return np.sqrt((self.gamma * self.pressure) / self.density)

    @property
    def alfven_speed(self):
        """ 
        """
        
        B = self.magnetic_field

        return np.sqrt(dot(B, B) / (mu0 * self.density))

    @property
    def viscous_tensor(self):
        """
        Defines the viscous tensor $\tau$:

        .. math::
            \tau_{kl} = \frac{1}{2}(\rho_0 + \rho_1)[\nu_k(v_l)\frac{\partial v_l}{\partial x_k} + \nu_l(v_k)\frac{\partial v_k}{\partial x_l}]
        """

        rho = self.density
        v = self.velocity
        #self.set_viscosity(v)
        visc = self.total_viscosity(v[0])
        # Define a new solver to differentiate individial velocity vectors.
        v_solver = Solver(self.solver.dx)
        
        # So very unsure about this equation right here
        visctens = np.zeros(shape=(3, 3, *self.grid_size)) * (u.m**2 / u.s**2)
        """for k in range(3):
            for l in range(3):
                try:
                    visctens[k, l] = ((visc[k, l] * cd4(v[l], k, h[k])) \
                                   + (visc[l, k] * cd4(v[k], l, h[l]))).to(visctens.unit)
                except IndexError:
                    visctens[k, l] = 0 * visctens.unit"""
        
        visctens[0, 0] = (visc[0] * v_solver(v[0], 0)) * 2# + (visc[0] * v_solver(v[0], 0))
        visctens *= 0.5 * rho

        assert visctens.shape == (3, 3, *self.grid_size), \
            "Viscous tensor calculated with incorrect shape: {}, should be {}".format(visctens.shape, (3, 3, *self.grid_size))

        return visctens

    @property
    def shock_viscosity(self):
        """ 
        """

        c = 0.5
        delv = div(self.velocity, self.solver)
        delv[np.where(delv > 0)] = 0

        return c * (self.solver.dx**2) * abs(delv)

    def hyperdiff_viscosity(self, param, paramaxis):
        """ 
        """

        """if param.unit.si == u.kg / (u.m**2 * u.s):
            c = 0.4 * u.m**2
        else:
            c = 0.04 * u.m**2
        # New solvers for 1st and 3rd derivatives
        solve1 = Solver(self.solver.dx, 'forward', deriv=1, acc=1)
        solve3 = Solver(self.solver.dx, 'forward', deriv=3, acc=1)
        solve3.coeffs = [(-1, 1), (0, -3), (1, 3), (2, -1)]"""
        #print(solve3.coeffs)

        vt = self.alfven_speed.max() + self.sound_speed.max()
        #vt = self.alfven_speed + self.sound_speed
        #vt = self.alfven_speed + self.sound_speed + self.speed
        """d3 = windowmax(abs(solve3(param, paramaxis)), 0)
        d1 = windowmax(abs(solve1(param, paramaxis)), 0)
        ratio = np.zeros(d3.shape) * (d3.unit / d1.unit)
        ratio[d1 != 0] = d3[d1 != 0] / d1[d1 != 0]"""
        #print('\nratio', ratio.unit.si)

        #visc = c * self.solver.dx * vt * ratio
        visc = self.solver.dx * vt
        #print('\n\nhyperdiff', visc.shape, visc.unit.si, '(', param.unit.si, ')')
        return visc

    def total_viscosity(self, param, paramaxis=0):
        """ 
        """

        return self.hyperdiff_viscosity(param, paramaxis) + self.shock_viscosity

    @property
    def epsilon(self):
        """ 
        """

        nu = self.total_viscosity(self.magnetic_field/np.sqrt(mu0))

        eps = nu * self.solver(self.magnetic_field/np.sqrt(mu0), 0)
        return eps


class MHD_SAC():
    """
    """
    def __init__(self, grid_size, dx, gamma=5/3):
        """ 
        """

        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.gamma = gamma

        # Initiate empty arrays for core variables
        self._density_bg = np.zeros(grid_size) * u.kg / u.m**3
        self._density_pert = np.zeros(grid_size) * u.kg / u.m**3
        self._density = np.zeros(grid_size) * u.kg / u.m**3
        
        self._momentum_bg = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)
        self._momentum_pert = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)
        self._momentum = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)

        self._magnetic_field_bg = np.zeros((3, *grid_size)) * u.Tesla
        self._magnetic_field_pert = np.zeros((3, *grid_size)) * u.Tesla
        self._magnetic_field = np.zeros((3, *grid_size)) * u.Tesla

        self._energy_bg = np.zeros(grid_size) * u.J / u.m**3
        self._energy_pert = np.zeros(grid_size) * u.J / u.m**3
        self._energy = np.zeros(grid_size) * u.J / u.m**3

        # Collect names of all variables into a list
        # This is used to allow the parent Simulation object to access these variables.
        self.variables = ['density', #'density_bg', 'density_pert',
                          'momentum', #'momentum_bg', 'momentum_pert',
                          'magnetic_field', #'magnetic_field_bg', 'magnetic_field_pert',
                          'energy', #'energy_bg', 'energy_pert',
                          'velocity',
                          'magnetic_pressure', #'magnetic_pressure_bg', 'magnetic_pressure_pert',
                          'kinetic_pressure', #'kinetic_pressure_bg', 'kinetic_pressure_pert',
                          'pressure', #'pressure_bg', 'pressure_pert',
                         ]

        # Collect appropriate equations into a nice easy-to-use list
        self.equations = [self._ddt_density, self._ddt_momentum, self._ddt_energy, self._ddt_magfield]

    def _ddt_density(self, t, density=None):
        """
        """
        if not density:
            density = self.density
        return div(self.velocity * density, self.solver)

    def _ddt_momentum(self, t, momentum=None):
        """
        """
        if not momentum:
            momentum = self.momentum
        B0 = self.magnetic_field_bg / np.sqrt(mu0)
        B1 = self.magnetic_field_pert / np.sqrt(mu0)
        v = self.velocity

        return (-grad(self.pressure_pert, self.solver) \
                + tensordiv(vdp(B1, B0) + vdp(B0, B1), self.solver) \
                - tensordiv(vdp(v, momentum), self.solver))

    def _ddt_energy(self, t, energy=None):
        """
        """
        if not energy:
            energy = self.energy
        B0 = self.magnetic_field_bg / np.sqrt(mu0)
        B1 = self.magnetic_field_pert / np.sqrt(mu0)
        v = self.velocity
        
        return -div(
            (v*energy) - (B1 * dot(B1, v)) + (v*self.pressure_pert)
            - tv_dot(vdp(B1, B0) + vdp(B0, B1), v)
            + (self.pressure_bg * v)
            - (B0 * dot(B0, v)), self.solver)

    def _ddt_magfield(self, t, magfield=None):
        """
        """
        if not magfield:
            B = self.magnetic_field / np.sqrt(mu0)
        else:
            B = magfield / np.sqrt(mu0)
        v = self.velocity

        return -tensordiv(vdp(v, B) - vdp(B, v), self.solver) * np.sqrt(mu0)

    """
    Define getters and setters for variables, including for background and perturbed components
    """
    ## ==== Core variables ====
    # Density
    @property
    def density_bg(self):
        return self._density_bg

    @density_bg.setter
    @u.quantity_input
    def density_bg(self, density: u.kg/u.m**3):
        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density_bg = density

    @property
    def density_pert(self):
        return self._density_pert

    @density_pert.setter
    @u.quantity_input
    def density_pert(self, density: u.kg/u.m**3):
        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density_pert = density

    @property
    def density(self):
        return self.density_bg + self.density_pert

    @density.setter
    @u.quantity_input
    def density(self, density: u.kg/u.m**3):
        """
        Sets the simulation's density profile to the specified array.
        Other arrays which depend on the density values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        density : ndarray
            Array of density values. Shape must be (2, x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            density[0] should contain the background density component, and the perturbed component
            should be in density[1]

        """

        assert density.shape == (2, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._density_bg = density[0]
        self._density_pert = density[1]

    # Momentum
    @property
    def momentum(self):
        return self._momentum

    @momentum.setter
    @u.quantity_input
    def momentum(self, momentum: u.kg/(u.m**2 * u.s)):
        """
        Sets the simulation's velocity profile to the specified array.
        Other arrays which depend on the velocity values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        momentum : ndarray
            Array of momentum values. Shape must be equal to grid size of the simulation.
            The specified momentum defines the perturbed component of the momentum, since the
            background component is 0 by definition.

        """
        assert momentum.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._momentum = momentum

    # Magnetic field
    @property
    def magnetic_field_bg(self):
        return self._magnetic_field_bg

    @magnetic_field_bg.setter
    @u.quantity_input
    def magnetic_field_bg(self, magnetic_field: u.Tesla):
        assert magnetic_field.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._magnetic_field_bg = magnetic_field

    @property
    def magnetic_field_pert(self):
        return self._magnetic_field_pert

    @magnetic_field_pert.setter
    @u.quantity_input
    def magnetic_field_pert(self, magnetic_field: u.Tesla):
        assert magnetic_field.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._magnetic_field_pert = magnetic_field

    @property
    def magnetic_field(self):
        return self._magnetic_field_bg + self._magnetic_field_pert

    @magnetic_field.setter
    @u.quantity_input
    def magnetic_field(self, magnetic_field: u.Tesla):
        """
        Sets the simulation's magnetic field profile to the specified array.
        Other arrays which depend on the magnetic field, such as the magnetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------
        magnetic_field : ndarray
            Array of magnetic field values. Shape must be (2, x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            magnetic_field[0] should contain the background density component, and the perturbed
            component should be in magnetic_field[1]
        """

        assert magnetic_field.shape == (2, 3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._magnetic_field_bg = density[0]
        self._magnetic_field_pert = magnetic_field[1]

    # Internal energy
    @property
    def energy_bg(self):
        return self._energy_bg

    @energy_bg.setter
    @u.quantity_input
    def energy_bg(self, energy: u.J/u.m**3):
        assert energy.shape == self.grid_size, \
            'Specified energy density array shape does not match simulation grid.'
        self._energy_bg = energy

    @property
    def energy_pert(self):
        return self._energy_pert

    @energy_pert.setter
    @u.quantity_input
    def energy_pert(self, energy: u.J/u.m**3):
        assert energy.shape == self.grid_size, \
            'Specified energy density array shape does not match simulation grid.'
        self._energy_pert = energy

    @property
    def energy(self):
        return self._energy_bg + self._energy_pert

    @energy.setter
    @u.quantity_input
    def energy(self, energy: u.J/u.m**3):
        """
        Sets the simulation's total energy density profile to the specified array.
        Other arrays which depend on the energy values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------
        density : ndarray
            Array of energy values. Shape must be (2, x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            energy[0] should contain the background density component, and the perturbed component
            should be in energy[1]
        """

        assert energy.shape == (2, *self.grid_size), \
            'Specified energy density array shape does not match simulation grid.'
        shape, unit = self._energy.shape, self._energy.unit
        self._energy = Parameter(shape, unit, value=energy.to(unit).value)

    @property
    def core_variables(self):
        """
        """
        return [self.density_pert, self.momentum, self.energy_pert, self.magnetic_field_pert]

    ## ==== Derived variables ====
    # Velocity
    @property
    def velocity(self):
        """
        Returns the velocity profile of the simulation, as calculated from the momentum and total
        (background plus perturbed) density.
        """

        return self.momentum / self.density

    @velocity.setter
    @u.quantity_input
    def velocity(self, velocity: u.m / u.s):
        assert velocity.shape == (3, *self.grid_size), \
          'Specified velocity array shape does not match simulation grid.'
        self.momentum = velocity * self.density

    # Magnetic pressure
    @property
    def magnetic_pressure_bg(self):
        B0 = self.magnetic_field_bg / np.sqrt(mu0)
    
        return dot(B0, B0) / 2

    @property
    def magnetic_pressure_pert(self):
        B0 = self.magnetic_field_bg / np.sqrt(mu0)
        B1 = self.magnetic_field_pert / np.sqrt(mu0)

        return (dot(B1, B1) / 2) + dot(B0, B1)

    @property
    def magnetic_pressure(self):
        """
        Returns the simulation's magnetic pressure profile.

        The magnetic pressure is defined as:

        .. math::

            \\frac{\\textbf{B}_1^2}{2} + \\textbf{B}_0 \\textbf{B}_1

        where :math:`\\textbf{B}_0` and :math:`\\textbf{B}_1` are the background and perturbed magnetic field,
        respectively.
        """
        # TODO: docstring equation above needs some mu0s in it

        return self.magnetic_pressure_bg + self.magnetic_pressure_pert

    @property
    def kinetic_pressure_bg(self):
        return (self.gamma - 1) * (self.energy_bg - self.magnetic_pressure_bg)

    @property
    def kinetic_pressure_pert(self):
        v = self.velocity
        
        return (self.gamma - 1) * (self.energy_pert - ((self.density * dot(v, v)) / 2)  \
            - self.magnetic_pressure_pert)

    @property
    def kinetic_pressure(self):
        """
        Sets the simulation's kinetic pressure profile to the specified array.
        The total pressure, which depends on the kinetic pressure, is redefined automatically.

        The kinetic pressure is defined as:

        .. math::

            (\\gamma - 1) (e_0 - \\frac{(\\rho_0+\\rho_1)\\textbf{v}^2}{2} - \\textbf{B}_0\\textbf{B}_1 - \\frac{\\textbf{B}_1^2}{2})
        
        where the subscripts 0 and 1 indicate the background and perturbed magnetic field,
        respectively.
        """
        # TODO: docstring equation above needs some mu0s in it

        return self.kinetic_pressure_bg + self.kinetic_pressure_pert

    # Total pressure
    @property
    def pressure_bg(self):
        return ((self.gamma - 1) * self.energy_bg) - ((self.gamma - 2) * self.magnetic_pressure_bg)

    @property
    def pressure_pert(self):
        v = self.velocity

        return ((self.gamma - 1) * (self.energy_pert - ((self.density * dot(v, v)) / 2))) \
             - ((self.gamma - 2) * self.magnetic_pressure_pert)

    @property
    def pressure(self):
        """
        Returns the total pressure in the simulation domain by adding the kinetic and magnetic pressures.
        """
        
        return self.pressure_bg + self.pressure_pert

    @property
    def sound_speed(self):
        """
        Calculate the sound speed everywhere in the domain based on the pressure, density and adiabatic
        index:

        .. math::

            c_s = \\sqrt{\\frac{\\gamma p}{\\rho}}

        """

        return np.sqrt((self.gamma * self.total_pressure.sum(axis=0)) / self.density.sum(axis=0))
