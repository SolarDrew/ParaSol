from operators import *
import astropy.units as u

mu0 = np.pi * 4.0e-7 * (u.newton / (u.amp**2))


class AdiabaticHD():
    """Physics class fo adiabatic hydrodynamics.
    """
    #@u.quantity_input(adiabatic=u.m/u.s**2)
    def __init__(self, grid_size, gamma=5/3, adiabatic=1):#*u.m/u.s**2):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.gamma = gamma
        self.adiabatic_constant = adiabatic

        # Initiate empty arrays for core variables
        self._density = np.zeros(grid_size) * u.kg / u.m**3
        self._momentum = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)

        # Collect names of all variables into some lists
        self._variable_names = ['density', 'momentum', # Core equation variables
                                'velocity', 'pressure'] # Derived variables

        # Collect equations into a nice easy-to-use list
        self.equations = [self._ddt_density, self._ddt_momentum]

    def _ddt_density(self, t, density=None):
        """
        """
        if not density:
            density = self.density
        return -div(self.velocity * density, self.solver)

    def _ddt_momentum(self, t, momentum=None):
        """
        """
        if not momentum:
            momentum = self.momentum
        v = self.velocity
        #print('::::', self.pressure.unit, ':', v.unit, ':', momentum.unit)
        delp = -grad(self.pressure, self.solver)
        #print('----', v.unit, '--', momentum.unit, '--', vdp(v, momentum).unit)
        divvm = -tensordiv(vdp(v, momentum), self.solver)
        #print(';;;;', delp.unit, ';', divvm.unit)

        return (-grad(self.pressure, self.solver) \
                - tensordiv(vdp(v, momentum), self.solver))

    """
    Define getters and setters for variables.
    """
    ## ==== Core variables ====
    # Density
    @property
    def density(self):
        return self._density

    @density.setter
    @u.quantity_input
    def density(self, density: u.kg/u.m**3):
        """Sets the simulation's density profile to the specified array.
        Other arrays which depend on the density values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        density : ndarray
            Array of density values. Shape and size must be equal to those of the simulation grid.
            Must have units of density.

        """

        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density = density

    # Momentum
    @property
    def momentum(self):
        return self._momentum

    @momentum.setter
    @u.quantity_input
    def momentum(self, momentum: u.kg/(u.m**2 * u.s)):
        """Sets the simulation's momentum profile to the specified array.
        Other arrays which depend on the velocity values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        momentum : ndarray
            Array of momentum vectors. Shape must be (3, x, [y, z]), where x, y and z are the dimensions of the simulation grid.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """
        assert momentum.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._momentum = momentum

    @property
    def core_variables(self):
        """Returns an up-to-date list of the core variables used in the calculations.

        """
        return [self.density, self.momentum]

    ## ==== Derived variables ====
    # Velocity
    @property
    def velocity(self):
        """Returns the velocity profile of the simulation, as calculated from the momentum and total density.
        """

        return self.momentum / self.density

    @velocity.setter
    @u.quantity_input
    def velocity(self, velocity: u.m / u.s):
        """Defines the velocity throughout the simulation, and automatically updates the momentum based on the current density values.

        Parameters
        ----------

        velocity : ndarray
            Array of velocity vectors with shape (3, x, [y, z]) where x, y and z are the spatial grid sizes of the simulation.
            Note that a full 3D vector is required even if the simulation is run for fewer than 3 dimensions.
            Must have units of velocity.

        """
        assert velocity.shape == (3, *self.grid_size), \
          'Specified velocity array shape does not match simulation grid.'
        self.momentum = velocity * self.density

    @property
    def pressure(self):
        """Returns the simulation's pressure, defined as:

        .. math::

            p = c \\rho^\\gamma
        
        """
        rho = self.density
        c = self.adiabatic_constant
        #print('####', rho[0], '#', c, '#', self.gamma)

        p = c * (rho.value ** self.gamma)#*rho.unit
        #print('####', p.unit)

        return p * u.kg / (u.m * u.s**2)


class HD():
    """Physics class for hydrodynamics.

    This class defines the Euler equations for fluid dynamics:

    .. math::

       \\frac{\\partial \\rho}{\\partial t} + \\nabla \\cdot (\\vec{v} \\rho) = 0
       \\frac{\\partial (\\rho \vec{v})}{\\partial t} + \\nabla \\cdot (\\vec{v} \\rho \\vec{v}) + \\nabla p = 0
       \\frac{\\partial e}{\\partial t} + \\nabla \\cdot (\\vec{v} e + \\vec{v} p) = 0

    for the fluid density, :math:`\\rho`, momentum, :math:`\\vec{m} = \\vec{v} \\rho`, energy :math:`e` and kinetic pressure :math:`p`.
    The pressure is a derived quantity defined as

    .. math::
       p = (\\gamma - 1) (e - \\frac{\\rho \\vec{v}^2}{2})

    Parameters
    ----------
    grid_size : tuple of ints
        Tuple of 1, 2 or 3 values defining the size of the simulation grid.
    gamma : float
        Value of the adiabatic index.

    Attributes
    ----------
    grid_size : tuple of ints
        Size of the simulation grid, as defined at initiation.
    gamma : float
        Adiabatic index for the simulation.

    See Also
    --------
    Base physics class?

    References
    ----------

    Examples
    --------

    """
    def __init__(self, grid_size, gamma=5/3):
        """
        """
        # Domain size
        self.grid_size = grid_size

        # Physical parameters
        self.gamma = gamma

        # Initiate empty arrays for core variables
        self._density = np.zeros(grid_size) * u.kg / u.m**3
        self._momentum = np.zeros((3, *grid_size)) * u.kg / (u.m**2 * u.s)
        self._energy = np.zeros(grid_size) * u.J / u.m**3

        # Collect names of all variables into some lists
        self._variable_names = ['density', 'momentum', 'energy', # Core equation variables
                                'velocity', 'pressure'] # Derived variables

        # Collect equations into a nice easy-to-use list
        self.equations = [self._ddt_density, self._ddt_momentum, self._ddt_energy]

    def _ddt_density(self, t, density=None):
        """
        """
        if not density:
            density = self.density
        return -div(self.velocity * density, self.solver)

    def _ddt_momentum(self, t, momentum=None):
        """
        """
        if not momentum:
            momentum = self.momentum
        v = self.velocity

        return (-grad(self.pressure, self.solver) \
                - tensordiv(vdp(v, momentum), self.solver)) # momentum * rho?

    def _ddt_energy(self, t, energy=None):
        """
        """
        if not energy:
            energy = self.energy
        v = self.velocity
        
        return -div((v*energy) + (v*self.pressure), self.solver)

    """
    Define getters and setters for variables.
    """
    ## ==== Core variables ====
    # Density
    @property
    def density(self):
        return self._density

    @density.setter
    @u.quantity_input
    def density(self, density: u.kg/u.m**3):
        """Sets the simulation's density profile to the specified array.
        Other arrays which depend on the density values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        density : ndarray
            Array of density values. Shape and size must be equal to those of the simulation grid.
            Must have units of density.

        """

        assert density.shape == self.grid_size, \
            'Specified density array shape does not match simulation grid.'
        self._density = density

    # Momentum
    @property
    def momentum(self):
        return self._momentum

    @momentum.setter
    @u.quantity_input
    def momentum(self, momentum: u.kg/(u.m**2 * u.s)):
        """Sets the simulation's momentum profile to the specified array.
        Other arrays which depend on the velocity values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        momentum : ndarray
            Array of momentum vectors. Shape must be (3, x, [y, z]), where x, y and z are the dimensions of the simulation grid.
            Note that a full 3D vector is necessary even if the simulation has fewer than 3 dimensions.

            TODO: Figure out if this 'feature' is breaking things and if it can be removed.

        """
        assert momentum.shape == (3, *self.grid_size), \
            'Specified density array shape does not match simulation grid.'
        self._momentum = momentum

    # Internal energy
    @property
    def energy(self):
        return self._energy

    @energy.setter
    @u.quantity_input
    def energy(self, energy: u.J/u.m**3):
        """Sets the simulation's total energy density profile to the specified array.
        Other arrays which depend on the energy values, such as the kinetic pressure,
        are then redefined automatically.
        
        Parameters
        ----------

        energy : ndarray
            Array of energy values. Shape must be (x, y, z), where x, y, and z are the grid
            sizes of the simulation in the x, y, and z dimensions.
            Must have units of energy.

        """

        assert energy.shape == self.grid_size, \
            'Specified energy density array shape does not match simulation grid.'
        shape, unit = self._energy.shape, self._energy.unit
        self._energy = energy

    @property
    def core_variables(self):
        """Returns an up-to-date list of the core variables used in the calculations.

        """
        return [self.density, self.momentum, self.energy]

    ## ==== Derived variables ====
    # Velocity
    @property
    def velocity(self):
        """Returns the velocity profile of the simulation, as calculated from the momentum and total density.
        """

        return self.momentum / self.density

    @velocity.setter
    @u.quantity_input
    def velocity(self, velocity: u.m / u.s):
        """Defines the velocity throughout the simulation, and automatically updates the momentum based on the current density values.

        Parameters
        ----------

        velocity : ndarray
            Array of velocity vectors with shape (3, x, [y, z]) where x, y and z are the spatial grid sizes of the simulation.
            Note that a full 3D vector is required even if the simulation is run for fewer than 3 dimensions.
            Must have units of velocity.

        """
        assert velocity.shape == (3, *self.grid_size), \
          'Specified velocity array shape does not match simulation grid.'
        self.momentum = velocity * self.density

    @property
    def pressure(self):
        """Sets the simulation's kinetic pressure profile to the specified array.

        The kinetic pressure is defined as:

        .. math::

            p = (\\gamma - 1) (e_0 - \\frac{\\rho\\textbf{v}^2}{2})
        
        """
        v = self.velocity

        return (self.gamma - 1) * (self.energy - ((self.density * dot(v, v)) / 2))


    @property
    def sound_speed(self):
        """Calculate the sound speed everywhere in the domain based on the pressure, density and adiabatic
        index:

        .. math::

            c_s = \\sqrt{\\frac{\\gamma p}{\\rho}}

        """

        return np.sqrt((self.gamma * self.pressure) / self.density)


