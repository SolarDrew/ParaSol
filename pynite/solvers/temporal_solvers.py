import numpy as np

def euler_simple(self):
    """
    """
    derivs = []
    for eq in self.physics.equations:
        derivs.append(eq(self.current_time) * self.dt)

    for f, df in zip(self.physics.core_variables, derivs):
        np.add(f, df, out=f)


def euler_mod(self):
    """ 
    This may be wrong somewhere along the line because it doesn't behave in the same way as RK4.
    Could just be an accuracy thing, but compare the methods first.
    """
    half_dt = self.dt / 2
    mid = []
    derivs = []
    for eq in self.physics.equations:
        mid.append(np.array([eq(self.current_time) * half_dt]))
    for f, df in zip(self.physics.core_variables, mid):
        np.add(f, df, out=df)

    for f, eq in zip(mid, self.physics.equations):
        derivs.append(eq(self.current_time+half_dt, f) * self.dt)
    for f, df in zip(self.physics.core_variables, derivs):
        np.add(f, df, out=f)


def euler_imp(self):
    """
    """
    end = []
    derivs = []
    for eq in self.physics.equations:
        end.append(np.array([eq(self.current_time) * self.dt]))
    for f, df in zip(self.physics.core_variables, end):
        np.add(f, df, out=df)

    for f, eq in zip(end, self.physics.equations):
        derivs.append(((eq(self.current_time) + eq(self.current_time + self.dt, end)) / 2) * self.dt)
    for f, df in zip(self.physics.core_variables, derivs):
        np.add(f, df, out=f)


def runge_kutta2(self):
    """
    """
    derivs = []
    for eq in self.physics.equations:
        derivs.append(eq(self.current_time) * self.dt)

    for i, (f, k1, eq) in enumerate(zip(self.physics.core_variables, derivs, self.physics.equations)):
        derivs[i] += eq(self.current_time+self.dt, f + k1) * self.dt

    for f, df in zip(self.physics.core_variables, derivs):
        np.add(f, df/2, out=f)

    #k1 = ddt(t) * dt
    #k2 = ddt(t + dt, var + k1) * dt

    #return (k1 + k2) / 2


def runge_kutta4(self):
    """
    """
    half_dt = self.dt / 2
    kn = []
    derivs = []
    for eq in self.physics.equations:
        k1 = eq(self.current_time) * self.dt
        kn.append(k1)
        derivs.append(k1)
    for f, k1 in zip(self.physics.core_variables, kn):
        np.add(f, k1/2, out=f)

    for i, (f, k1, eq) in enumerate(zip(self.physics.core_variables, kn, self.physics.equations)):
        k2 = eq(self.current_time + half_dt, f) * self.dt
        kn[i] = k2
        derivs[i] += (2 * k2)
    for f, f0, k2 in zip(self.physics.core_variables, self.physics._orig_variables, kn):
        np.add(f0, k2/2, out=f)

    for i, (f, k2, eq) in enumerate(zip(self.physics.core_variables, kn, self.physics.equations)):
        k3 = eq(self.current_time + half_dt, f) * self.dt
        kn[i] = k3
        derivs[i] += (2 * k3)
    for f, f0, k3 in zip(self.physics.core_variables, self.physics._orig_variables, kn):
        np.add(f0, k3, out=f)

    for i, (f, k3, eq) in enumerate(zip(self.physics.core_variables, kn, self.physics.equations)):
        derivs[i] += eq(self.current_time+self.dt, f) * self.dt

    for f, f0, df in zip(self.physics.core_variables, self.physics._orig_variables, derivs):
        np.add(f0, df/6, out=f)
