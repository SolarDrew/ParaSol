# Pynite

<!-- Synopsis -->

Pynte is a generic finite difference solving library written entirely in Python. It provides several spatial and temporal solvers, and a number of physics objects to solve for different sets of equations (hydrodynamics, MHD, etc.).

<!-- Code Example -->

<!-- Motivation -->

The purpose of Pynite is to provide a finite difference solver which is easier to use and set up than many which use compiled languages such as Fortran or C.

## Installation

<!-- Tests -->

## Contributing
PRs are always welcome. At some point I will set up some developer instructions and an issue page, and I'll link those here.

## License
BSD 3-Clause