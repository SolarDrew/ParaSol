import numpy as np
import astropy.units as u

class torsional_alfven():
    def __init__(self, A, k, omega):
        """
        """
        self.amplitude = A
        self.wavenumber = k
        self.frequency = omega

    def __call__(self, sim):
        """
        """
        A = self.amplitude
        k = self.wavenumber
        omega = self.frequency

        x, y, z = sim.coords
        kx = k * (y + z)
        wt = omega * sim.current_time
        kxwt = (kx - wt).value
    
        bx = A * np.cos(kxwt)
        by = (1 + (A * np.sin(kxwt))) / np.sqrt(2)
        bz = (1 - (A * np.sin(kxwt))) / np.sqrt(2)
    
        vx = -A * np.cos(kxwt)
        vy = -(A * np.sin(kxwt)) / np.sqrt(2)
        vz = -(A * np.sin(kxwt)) / np.sqrt(2)
    
        sim.physics.magnetic_field_bg = np.array([bx, by, bz]) * u.T
        sim.physics.velocity = np.array([vx, vy, vz]) * u.m / u.s
