import numpy as np
import astropy.units as u
from astropy.utils.console import ProgressBar

from physics import MHD
from solvers.spatial_solvers import Solver
from solvers.temporal_solvers import *

import warnings
import types

warnings.filterwarnings('error')

"""
Defines the base Simulation class used to initiate the domain and parameters for a new simulation.
"""

class Simulation():
    """Provides a simulation grid of the given size.

    Parameters
    ----------

    xrange, yrange, zrange : ndarray
        Arrays of x, y and z coordinates used to define the simulation grid.
        For a 3D simulation, all of these values must be supplied, but for one or two dimensions the appropriate ranges can be left out.
        In this case they will default to None.

    rangeunit : astropy.unit.Unit
        Units of the domain sizes specified by ``xrange``, ``yrange`` and ``zrange``.
        Must be units describing length, otherwise everything will break horribly.

    physics : Pynite Physics object
        Object defining the equations of the physics used for the simulation.

    physpars : dict
        Dictionary of parameters passed to the Physics object.

    driver : Driver object?
        Not yet properly implemented

    time_stepper : func
        Function defining the temporal evolution method used for the simulation.

    dt : astropy.units.Quantity
        Time-step value, must have units of time.


    Examples
    --------

    >>> mysim = Simulation(xrange=np.linspace(-0.5, 0.5, 128),
                           yrange=np.linspace(-0.5, 0.5, 128),
                           zrange=np.linspace(0.0, 1.0, 128))
    """
    @u.quantity_input(dt=u.s)
    def __init__(self, xrange, yrange=None, zrange=None, rangeunit=u.m,
                       physics=MHD, physpars={}, driver=None,
                       time_stepper=None, dt=1.0*u.s):

        # Initiate useful information about the simulation
        ranges = [irange for irange in (xrange, yrange, zrange) if irange is not None]
        self.coords = np.meshgrid(*ranges, indexing='ij') * rangeunit
        self.grid_size = self.coords.shape[1:]
        self.stepsize = [range[1] - range[0] for range in ranges] * rangeunit
        self.current_time = 0 * u.s
        self.start_time = 0 * u.s
        self.end_time = 0 * u.s
        self.current_iteration = 0

        # Link the appropriate peripherals to the Simulation:
        # Physics object, numerical solver, time-stepper and driver
        #print(physpars)
        self.physics = physics(self.grid_size, **physpars)
        self.physics.solver = Solver(self.stepsize)
        self.driver = driver
        self.dt = dt
        if not time_stepper:
            self.time_stepper = types.MethodType(runge_kutta4, self)
        else:
            self.time_stepper = types.MethodType(time_stepper, self)

        # Courant parameter equivalent that controls the time-step
        self.courantpar = 0.8

    @u.quantity_input(max_time=u.s)
    def run_simulation(self, max_its=np.inf, max_time=np.inf*u.s):
        """Runs the simulation as set up, either for the given number of iterations or until the simulation reaches the given time.
        
        Parameters
        ----------
        max_its : int
            Tells the simulation to run for a set number of iterations.

        max_time : astropy.units.Quantity
            Maximum total (in-simulation) time to allow the simulation to run.
            Must have units of time.


        Examples
        --------
        >>> # Run a simulation for exactly one thousand iterations.
        >>> mysim.run_simulation(max_time=1000)

        >>> # Run a simulation for up to half an hour of simulation time.
        >>> mysim.run_simulation(max_time=30*u.minute)

        """
        if np.isinf(max_its) and np.isinf(max_time.value):
            print("Either max_time or max_its must be set.")
            return

        if np.isinf(max_time):
            pb = ProgressBar(max_its)
        else:
            pb = ProgressBar(int(max_time / self.dt))

        dt = self.dt

        with pb as bar:
          while self.current_iteration < max_its and self.current_time < max_time:
            if dt.value < 1e-100:
                break
            # Compute derived fields
            
            # Calculate time-step
            #dt = self.dt
    
            # Optional driver input
            if self.driver:
                self.driver(self)

            #print('\n')
            # Integration kernel
            """derivs = []
            for f, eq in zip(self.physics.core_variables, self.physics.equations):
                #print(self.physics.total_viscosity(f, 0).max().si)
                derivs.append(self.time_stepper(eq, dt, f, self.current_time))
                #print('++++', f.mean(), derivs[-1].mean())
            #print(self.physics.viscous_tensor[0, 0].max().si)
    
            # Boundary update kernel
    
            #print('\n')
            # Update next step
            for f, df in zip(self.physics.core_variables, derivs):
                #print('\n....', f.min(), df.min())
                np.add(f, df, out=f)
                #print('\t==>', f.min())"""

            self.physics._orig_variables = [var.copy() for var in self.physics.core_variables]
            self.time_stepper()

            # Advance time information on the simulation
            self.current_time += dt
            self.current_iteration += 1

            # Calculate time step for next iteration
            """try:
                dt = min(dt, 0.8 * (min(self.stepsize) / self.velocity.max()))
                self.dt = dt
            except (ValueError, AttributeError):
                pass"""

            bar.update()

    def __getattr__(self, name):
        """
        Allows the user to treat variables within the physics object as attributes of the Simulation.
        E.g., mysim.velocity will fail initially, then this function will be called and return 
        mysim.physics.velocity.
        """

        if name in self.physics._variable_names:
            return getattr(self.physics, name)
        else:
            print('Variable "{}" not found in physics object.'.format(name))
            print('Available variables:\n', self.physics._variable_names)
            raise AttributeError
