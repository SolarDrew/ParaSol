import numpy as np
import astropy.units as u
from solvers.spatial_solvers import *

#####
# MANY OF THESE CAN BE REPLACED WITH NUMPY EQUIVALENTS, WHICH WILL PROBABLY MAKE THE WHOLE THING SIGNIFICANTLY FASTER
#####

def laplacian(f, solver):
    """
    Calculates the gradient of a scalar field.

    Parameters
    ----------

    f : array-like, shape=(x, [y, z])
        1-, 2- or 3-dimensional scalar field.

    Returns
    -------

    laplace : astropy.units.Quantity, shape=(x, [y, z])
        Scalar field corresponding to the laplacian of the specified scalar field, :math:`a = \\nabla^2 f = \\nabla \\cdot \\nabla f`.

    """

    assert len(solver.dx) == len(f.shape), \
        "Number of grid step sizes ({}) != to dimensionality of field ({})".format(len(h), len(f.shape))

    return sum([solver(f, axis) for axis in range(len(f.shape))])


def dot(vec1, vec2):
    """
    Calculates the dot product of two arrays of vector quantities.
    TODO: Replace this everywhere with the new NumPy way of doing this.

    Parameters
    ----------

    vec1, vec2 : array-like, shape=(3, x, [y, z])
        Arrays of vector values in a 1D, 2D or 3D domain.

    Returns
    -------

    scalar : ndarray, shape=(x, [y, z])
        3D grid of scalar values which are the dot products of specified vectors,

        .. math::

           a = \\vec{v_1} \\cdot \\vec{v_2}

    """

    assert vec1.shape[0] == 3, "First argument provided is not a vector field"
    assert vec2.shape[0] == 3, "Second argument provided is not a vector field"
    assert vec1.shape == vec2.shape, \
        "Shapes of vectors provided do not match: {}/{}".format(vec1.shape, vec2.shape)

    product = np.sum(vec1 * vec2, axis=0)
    assert product.shape == vec1.shape[1:], \
        "Result calculated has shape {}, should be {}".format(product.shape, vec1.shape[1:])

    return product


def cross(vec1, vec2):
    """
    Calculates the cross product of two arrays of vector quantities.

    Parameters
    ----------

    vec1, vec2 : array-like, shape=(3, x, [y, z])
        Arrays of vector values in a 1D, 2D or 3D domain.

    Returns
    -------

    product : ndarray, shape=(3, x, [y, z])
        Vector field corresponding to the cross product of the specified vectors,

        .. math::

           \\vec{a} = \\vec{v_1} \\times \\vec{v_2}

    """
    
    assert vec1.shape[0] == 3, "First argument provided is not a vector field"
    assert vec2.shape[0] == 3, "Second argument provided is not a vector field"
    assert vec1.shape == vec2.shape, \
        "Shapes of vectors provided do not match: {}/{}".format(vec1.shape, vec2.shape)

    product = np.array((((vec1[1] * vec2[2]) - (vec1[2] * vec2[1])), \
                        ((vec1[2] * vec2[0]) - (vec1[0] * vec2[2])), \
                        ((vec1[0] * vec2[1]) - (vec1[1] * vec2[0]))))\
            * vec1.unit * vec2.unit
    assert product.shape == vec1.shape, \
        "Result calculated has shape {}, should be {}".format(product.shape, vec1.shape)

    return product


def grad(f, solver):
    """
    Calculates the gradient of a scalar field.

    Parameters
    ----------

    f : array-like, shape=(x, [y, z])
        1-, 2- or 3-dimensional scalar field.

    Returns
    -------

    gradient : astropy.units.Quantity, shape=(3, x, [y, z])
        Vector field corresponding to the gradient of the specified scalar field,

        .. math::

           \\vec{a} = \\nabla f

    """

    assert len(solver.dx) == len(f.shape), \
        "Number of grid step sizes ({}) != to dimensionality of field ({})".format(len(h), len(f.shape))

    gradient = np.zeros((3, *f.shape)) * f.unit / solver.dx.unit
    for dim in range(len(solver.dx)):
        gradient[dim] = solver(f, dim)

    return gradient


def div(vec, solver):
    """
    Calculates the divergence of a vector field.

    Parameters
    ----------

    vec : array-like, shape=(3, x, [y, z])
        3-dimensional vector field.

    Returns
    -------

    divergence : ndarray, shape=(x, [y, z])
        Scalar field of values corresponding to divergence of specified vector field,

        .. math::

           a = \\nabla \\cdot \\vec{v}

    """

    assert vec.shape[0] == 3, "First argument provided is not a vector field"
    assert len(solver.dx) == len(vec.shape[1:]), \
        "Number of grid step sizes ({}) != to dimensionality of field ({})".format(len(solver.dx), len(vec.shape[1:]))

    dims = range(len(solver.dx))
    divergence = sum([solver(vec[i], i) for i in dims])
    assert divergence.shape == vec.shape[1:], \
        "Output field has shape {}, should be {}".format(divergence.shape, vec.shape[1:])

    return divergence


def curl(vec, solver):
    """
    Calculates the curl of a vector field.

    Parameters
    ----------

    vec : array-like, shape=(3, x, [y, z])
        3-dimensional vector field.

    Returns
    -------

    curl : ndarray, shape=(3, x, [y, z])
        Vector field corresponding to the curl of the input vector field.

        .. math::

           \\vec{a} = \\nabla \\times \\vec{v}

    """

    assert vec.shape[0] == 3, "First argument provided is not a vector field"
    assert len(solver.dx) == len(vec.shape[1:]), \
        "Number of grid step sizes ({}) != to dimensionality of field ({})".format(len(solver.dx), len(vec.shape[1:]))

    curl = np.zeros(vec.shape) * vec.unit / solver.dx.unit
    for k in range(3):
        for l in range(3):
            for m in range(3):
                try:
                    curl[k] += levi_civita3d(k, l, m) * solver(vec[m], l, solver.dx[l])
                except IndexError:
                    pass

    return curl


def vdp(vec1, vec2):
    """
    Calculate the Vector Direct Product of two vectors.

    Parameters
    ----------

    vec1, vec2 : array-like, shape=(3, x, [y, z])
        Arrays of vector values in a 1D, 2D or 3D domain.

    Returns
    -------

    tensor : ndarray, shape=(3, 3, x, [y, z])
        Tensor field resulting from direct product of the specified vectors.

        .. math::

           \\textbf{A} = \\vec{v_1} \\vec{v_2}

    References
    ----------
    http://mathworld.wolfram.com/VectorDirectProduct.html

    """

    assert vec1.shape[0] == 3, "First argument provided is not a vector field"
    assert vec2.shape[0] == 3, "Second argument provided is not a vector field"
    assert vec1.shape == vec2.shape, \
        "Shapes of vectors provided do not match: {}/{}".format(vec1.shape, vec2.shape)

    tensor = vec1 * vec2.reshape(3, 1, *vec2.shape[1:])
    assert tensor.shape == (3, *vec1.shape), \
        "Output field has shape {}, should be {}".format(tensor.shape, (3, *vec1.shape))

    return tensor


def tv_dot(tensor, vec):
    """
    Calculate the dot product of a vector with a tensor.

    Parameters
    ----------
    tensor : array-like, shape=(3, 3, x, [y, z])
        Tensor field in the same space as the vector above.

    vec : array-like, shape=(3, x, [y, z])
        Array of vector values in a 1D, 2D or 3D domain.

    Returns
    -------
    dot : ndarray, shape=(3, x, [y, z])
        Resultant vector field.

        .. math::

           \\vec{a} = \\textbf{T} \\cdot \\vec{v}

    """

    assert tensor.shape[:2] == (3, 3), "First argument provided is not a tensor field"
    assert vec.shape[0] == 3, "Second argument provided is not a vector field"

    dims = range(len(vec.shape[1:]))
    dot = np.array([sum([tensor[0, i, ...] * vec[i] for i in dims]),
                    sum([tensor[1, i, ...] * vec[i] for i in dims]),
                    sum([tensor[2, i, ...] * vec[i] for i in dims])]) \
        * vec.unit * tensor.unit
    assert dot.shape == vec.shape, \
        "Output field has shape {}, should be {}".format(dot.shape, vec.shape)

    return dot


def vt_dot(vec, tensor):
    """ 
    Calculate the dot product of a vector with a tensor.

    Parameters
    ----------
    vec : array-like, shape=(3, x, [y, z])
        Array of vector values in a 1D, 2D or 3D domain.

    tensor : array-like, shape=(3, 3, x, [y, z])
        Tensor field in the same space as the vector above.

    Returns
    -------
    dot : ndarray, shape=(3, x, [y, z])
        Resultant vector field.
    """

    assert vec.shape[0] == 3, "First argument provided is not a vector field"
    assert tensor.shape[:2] == (3, 3), "Second argument provided is not a tensor field"

    dims = range(len(vec.shape[1:]))
    dot = np.array([sum([vec[i] * tensor[i, 0, ...] for i in dims]),
                    sum([vec[i] * tensor[i, 1, ...] for i in dims]),
                    sum([vec[i] * tensor[i, 2, ...] for i in dims])]) \
        * vec.unit * tensor.unit
    assert dot.shape == tensor.shape[1:], \
        "Output field has shape {}, should be {}".format(divergence.shape, tensor.shape[1:])

    return dot


def tensordiv(tensor, solver):
    """ 
    Calculates the divergence of a tensor field.

    Parameters
    ----------

    tensor : array-like, shape=(3, 3, x, [y, z])
        3-dimensional tensor field.

    Returns
    -------

    divergence : ndarray, shape=(3, x, [y, z])
        Vector field corresponding to the divergence of the input tensor field.

        .. math::

           \\vec{a} = \\nabla \\cdot \\textbf{T}

    """

    assert tensor.shape[:2] == (3, 3), "First argument provided is not a tensor field"
    assert len(solver.dx) == len(tensor.shape[2:]), \
        "Number of grid step sizes ({}) != to dimensionality of field ({})".format(len(solver.dx), len(tensor.shape[2:]))

    dims = range(len(solver.dx))
    divergence = np.array([sum([solver(tensor[i, 0, ...], i) for i in dims]),
                           sum([solver(tensor[i, 1, ...], i) for i in dims]),
                           sum([solver(tensor[i, 2, ...], i) for i in dims])]) \
               * tensor.unit / solver.dx.unit
    assert divergence.shape == tensor.shape[1:], \
        "Output field has shape {}, should be {}".format(divergence.shape, tensor.shape[1:])

    return divergence


def tensorcurl(tensor, solver):
    """
    Calculates the curl of a tensor field.

    Parameters
    ----------

    tensor : array-like, shape=(3, 3, x, [y, z])
        3-dimensional tensor field.

    Returns
    -------

    curl : ndarray, shape=(3, 3, x, [y, z])
        Tensor field corresponding to the curl of the input tensor field.

        .. math::

           \\textbf{A} = \\nabla \\times \\textbf{T}

    """

    assert tensor.shape[:2] == (3, 3), "First argument provided is not a tensor field"
    assert len(solver.dx) == len(tensor.shape[2:]), \
        "Number of grid step sizes ({}) != to dimensionality of field ({})".format(len(h), len(tensor.shape[2:]))

    curl = np.zeros(tensor.shape) * tensor.unit / solver.dx.unit
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    try:
                        curl[i, j, k] = (levi_civita3d(i, j, k) * solver(tensor[k, l], solver.dx[l], l)).to(curl.unit)
                    except IndexError:
                        curl[i, j, k] = 0

    assert curl.shape == tensor.shape, \
        "Output field has shape {}, should be {}".format(curl.shape, tensor.shape)

    return divergence


def fd_ratio(par, axis, h):
    """
    Calculates the ratio of the maxima of first- and third-order forward differences of the specified
    parameter for the full simulation grid.

    Parameters
    ----------

    par : Parameter
        Array of parameter values

    axis : int [0 | 1 | 2]
        Direction in which to calculate the ratio. 0, 1, and 2 correspond to the x, y, and z axes,
        respectively.

    h : float
        Distance between grid points on the specified axis.

    """

    if par.shape[0] == 2:
        par = par.sum(axis=0)

    if par.shape[0] == 3:
        par = np.sqrt((par**2).sum(axis=0))

    ratio = windowmax(abs(fd3(par, axis, h)), axis) / windowmax(abs(fd1(par, axis, h)), axis)
    if ratio[np.isfinite(ratio)].size == 0:
        return np.zeros(ratio.shape)
    else:
        return ratio


def windowmax(f, axis):
    """
    Find the maximum of the given array over a 5-point window for each point in the grid.

    Parameters
    ----------

    f : Parameter
        Array of values for which to find the 5-window maximum.

    axis : int [0 | 1 | 2]
        Direction in which to calculate the maxima. 0, 1, and 2 correspond to the x, y and z axes,
        respectively.

    """

    padding = [(0, 0)] * len(f.shape)
    padding[axis] = (4, 4)
    f = np.pad(f, padding, 'edge') * f.unit
    maxvals = np.array([f[shift(f.shape, i, axis)] for i in range(-1, 2)]).max(axis=0)

    return maxvals * f.unit


def levi_civita3d(i, j, k):
    if (i, j, k) == (0, 1, 2) or (1, 2, 0) or (2, 0, 1):
        return 1
    elif (i, j, k) == (2, 1, 0) or (0, 2, 1) or (1, 0, 2):
        return -1
    elif i ==j or j == k or k == i:
        return 0


def levi_civita2d(i, j):
    if (i, j) == (0, 1):
        return 1
    elif (i, j) == (1, 0):
        return -1
    elif i == j:
        return 0
