from setuptools import setup

setup(name='pynite',
      version=0.0,
      description='Python finite difference solver',
      url='',
      author='Drew Leonard',
      author_email='andy.j.leonard@gmail.com',
      license='BSD 3-Clause',
      packages=['pynite'],
      zip_safe=False)
